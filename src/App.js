import React, { Component } from 'react';
import './App.css';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      shoes: [],
    };
  }
  componentDidMount() {
    fetch("http://localhost:8000/api/shoe/")
    .then(res => res.json())
    .then(data => this.setState({shoes: data}))
  }
  render() {
    // Citation: https://www.youtube.com/watch?v=93p3LxR9xfM
    const shoesList = this.state.shoes.map((shoe) =>
      (
        <div key={shoe.id}>
          <ul>
          <h3>Shoe #{shoe.id}</h3>
          <li>Brand Name: {shoe.brand_name}</li>
          <li>Color: {shoe.color}</li>
          <li>Fasten Type: {shoe.fasten_type}</li>
          <li>Manufacturer: {shoe.manufacturer}</li>
          <li>Material: {shoe.material}</li>
          <li>Shoe Type: {shoe.shoe_type}</li>
          <li>Size: {shoe.size}</li>
          </ul>
        </div>
    ));
    return (
      <div>
        { shoesList }
      </div>
    )
  }
}


export default App;
